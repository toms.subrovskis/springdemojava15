package com.example.springdemojava15.selfCheck;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class Pistol implements Gun {

    private Logger logger = LoggerFactory.getLogger(Pistol.class);

    @Override
    public void shoot() {
        logger.info("Pistol is shooting");
    }
}
