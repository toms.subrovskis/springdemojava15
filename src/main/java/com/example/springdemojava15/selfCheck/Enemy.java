package com.example.springdemojava15.selfCheck;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Enemy {

    private Gun weapon;

    public Enemy(@Qualifier("bazooka") Gun weapon){
        this.weapon=weapon;
    }

    public void shoot(){
        weapon.shoot();
    }
}
