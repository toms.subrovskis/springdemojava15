package com.example.springdemojava15.selfCheck;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Person {

    private Gun laser;

    public Person(@Qualifier("lazer") Gun laser){
        this.laser=laser;
    }

    public void startShooting(){
        this.laser.shoot();
    }
}
