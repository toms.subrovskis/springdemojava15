package com.example.springdemojava15.selfCheck;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class Bazooka implements Gun {

    private Logger logger = LoggerFactory.getLogger(Bazooka.class);

    @Override
    public void shoot() {
        logger.info("Bazooka is shooting");
    }
}
