package com.example.springdemojava15.selfCheck;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShootingRange implements CommandLineRunner {

    private final Person person;
    private final Enemy enemy;
    private List<Gun> weapons;

    public ShootingRange(Person person, Enemy enemy, List<Gun> weapons){
        this.person=person;
        this.enemy=enemy;
        this.weapons=weapons;
    }

    @Override
    public void run(String... args) throws Exception {
        person.startShooting();
        enemy.shoot();
        weapons.stream().forEach( weapon -> System.out.println(weapon));
    }
}
