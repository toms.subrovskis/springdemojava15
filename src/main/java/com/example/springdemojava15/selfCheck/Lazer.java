package com.example.springdemojava15.selfCheck;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class Lazer implements Gun{

    private Logger logger = LoggerFactory.getLogger(Lazer.class);

    public void shoot(){
        logger.info("Laser is shooting");
    }
}
