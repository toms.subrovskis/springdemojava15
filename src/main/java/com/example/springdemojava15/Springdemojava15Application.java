package com.example.springdemojava15;

import com.example.springdemojava15.ex3.DummyLogger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Springdemojava15Application{

	public static void main(String[] args) {
		SpringApplication.run(Springdemojava15Application.class, args);
	}

//	@Bean
//	DummyLogger getLogger(){
//		return new DummyLogger() {
//			@Override
//			public void sayHello() {
//				System.out.println("Anonymous Bean class form Springdemojava15Application");
//			}
//		};
//	}
//
//	@Override
//	public void run(String... args) throws Exception {
//		getLogger().sayHello();
//	}
}
