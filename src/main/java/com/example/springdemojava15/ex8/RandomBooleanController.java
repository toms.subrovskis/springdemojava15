package com.example.springdemojava15.ex8;

import com.example.springdemojava15.ex6.MyCustomConfigurationComponent;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;


@RestController()
@RequestMapping("/api")
@RequiredArgsConstructor
public class RandomBooleanController {

    private final RandomBooleanProvider randomBooleanProvider;

    private final MyCustomConfigurationComponent myCustomConfigurationComponent;

    @GetMapping("/random-boolean")
    public Boolean getRandomBoolean(){
        return randomBooleanProvider.getValue();
    }

    @GetMapping("/conf")
    public MyCustomConfigurationComponent getConfiguration(){
        return this.myCustomConfigurationComponent;
    }
}
