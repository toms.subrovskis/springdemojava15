package com.example.springdemojava15.ex8;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.RequestScope;

import java.util.Random;

/**
 * Create a project based on Spring Boot using:
 *
 * spring-boot-starter-web
 * lombok
 * Implement a controller (REST) with an endpoint GET/api/random-boolean that returns a value using the RandomBooleanProvider bean and the getValue method. When making several HTTP requests in a row, let endpoint return a different value.
 *
 * Do this task without modifying the body of the RandomBooleanProvider class.
 *
 * Extra task if you have time:
 * Add method to expose configuration you created in Excersise 6 and imported from application.properties file.
 */

@Component
@RequestScope
public class RandomBooleanProvider {
    private final boolean value = new Random().nextBoolean();

    public boolean getValue() {
        return value;
    }
}
