package com.example.springdemojava15.ex3;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

@Service
//@RequiredArgsConstructor
public class MainLoggerHolder implements CommandLineRunner {

    private final DummyLogger dummyLogger;

    public MainLoggerHolder(DummyLogger dummyLogger){
        this.dummyLogger=dummyLogger;
    }

    @Override
    public void run(String... args) throws Exception {
        dummyLogger.sayHello();
    }
}

