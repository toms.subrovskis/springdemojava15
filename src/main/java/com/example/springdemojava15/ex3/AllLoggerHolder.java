package com.example.springdemojava15.ex3;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
//@RequiredArgsConstructor
public class AllLoggerHolder implements CommandLineRunner {

    private final List<DummyLogger> allDummyLoggers;

    public AllLoggerHolder(List<DummyLogger> loggers){
        this.allDummyLoggers=loggers;
    }

    @Override
    public void run(String... args) throws Exception {
        for (DummyLogger allDummyLogger : allDummyLoggers) {
            allDummyLogger.sayHello();
        }
    }
}