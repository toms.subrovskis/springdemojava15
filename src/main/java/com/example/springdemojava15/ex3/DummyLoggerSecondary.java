package com.example.springdemojava15.ex3;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DummyLoggerSecondary implements DummyLogger {
    @Override
    public void sayHello() {
        log.info("Secondary logger!");
    }
}

