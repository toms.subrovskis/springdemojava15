package com.example.springdemojava15.ex3;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

@Service
public class SecondaryLoggerHolder implements CommandLineRunner {

//    @Autowired
    private DummyLogger dummyLogger;

//    public SecondaryLoggerHolder(@Qualifier("dummyLoggerSecondary")DummyLogger dummyLogger) {
//        this.dummyLogger = dummyLogger;
//    }

    @Autowired
    public void setDummyLogger(@Qualifier("dummyLoggerSecondary")DummyLogger dummyLogger){
        this.dummyLogger=dummyLogger;
    }

    @Override
    public void run(String... args) throws Exception {
        dummyLogger.sayHello();
    }
}
