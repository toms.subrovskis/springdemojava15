package com.example.springdemojava15.ex6;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;
import java.util.List;
import java.util.Map;

/**
 * Create a project based on Spring Boot using:
 *
 * spring-boot-starter-web
 * lombok
 * spring-boot-starter-validation
 * On the basis of the following properties from the application.properties file, create a configuration class which represents a property group.
 *
 * pl.sdacademy.ex6.email=test@gmail.com
 * pl.sdacademy.ex6.first-name=Andrzej
 * pl.sdacademy.ex6.last-name=Nowak
 * pl.sdacademy.ex6.address=Nowakowskiego 1
 * pl.sdacademy.ex6.age=20
 * pl.sdacademy.ex6.values[0]=v1
 * pl.sdacademy.ex6.values[1]=v2
 * pl.sdacademy.ex6.custom-attributes.k1=v1
 * pl.sdacademy.ex6.custom-attributes.k2=v2
 * These properties should be validated at the start of the application and meet the following conditions:
 *
 * the pl.sdacademy.ex6.email field is required and must be a valid email address
 * the pl.sdacademy.ex6.age field is required and must be at least 18
 * the pl.sdacademy.ex6.last-name field is required and must be between 3 and 20 characters
 * the pl.sdacademy.ex6.address field is required and consists of 2 parts separated by a space
 * the pl.sdacademy.ex6.values collection is not empty
 * the pl.sdacademy.ex6.custom-attributes collection is not empty
 *
 */
@Component
@Validated
@ConfigurationProperties(prefix = "pl.sdacademy.ex6")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MyCustomConfigurationComponent {

    @Email(message = "wrong email")
    @NotNull
    private String email;
    private String firstName;
    @Length(min = 3, max = 20)
    @NotNull
    private String lastName;
    private String address;
    @Min(18)
    @NotNull
    private int age;
    @NotEmpty
    private List<String> values;
    @NotEmpty
    private Map<String,String> customAttributes;

    @AssertTrue
    private boolean isAddressValid(){
        return address.split(" ").length == 2;
    }
}
