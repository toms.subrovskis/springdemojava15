package com.example.springdemojava15.ex4;

import lombok.extern.slf4j.Slf4j;

/**
 * Create 3 beans in a class named UtilConfiguration:
 *
 * a bean named dummyLogger which is an implementation of theDummyLogger class
 * a bean named listUtility implementing the ListUtil class. Use the method name to establish the correct bean name.
 * bean named stringUtility which is an implementation of the StringUtil class. The method creating this bean should be named stringUtil.
 */

@Slf4j
public class DummyLogger {

    public void sayHi() {
        log.info("Hi from DummyLogger");
    }
}
