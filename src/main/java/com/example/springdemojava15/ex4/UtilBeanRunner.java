package com.example.springdemojava15.ex4;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UtilBeanRunner implements CommandLineRunner {

    private final ListUtil listUtil;
    private final StringUtil stringUtil;
    private final DummyLogger dummyLogger;

    @Override
    public void run(String... args) throws Exception {
        int result = listUtil.sumElements(List.of(2, 5));
        log.info(String.valueOf(result));

        String sentence = stringUtil.formSentence(List.of("best", "advice", "ever"));
        log.info(sentence);

        dummyLogger.sayHi();
    }
}
