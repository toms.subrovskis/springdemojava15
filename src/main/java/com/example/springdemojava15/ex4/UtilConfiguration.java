package com.example.springdemojava15.ex4;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UtilConfiguration {

    @Bean()
    public DummyLogger generateDummyLogger(){
        return new DummyLogger();
    }

    @Bean("listUtility")
    public ListUtil listUtility(){
        return new ListUtil();
    }

    @Bean("stringUtility")
    public StringUtil stringUtil(){
        return new StringUtil();
    }
}
