package com.example.springdemojava15.rest;

public class StudentNotFoundException extends RuntimeException {

    StudentNotFoundException(Long id){
        super("Could not find student with Id "+id);
    }
}
