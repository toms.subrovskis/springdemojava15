package com.example.springdemojava15.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.stream.Collectors;

@RestControllerAdvice
public class GlobalExceptionAdvice {

    @ExceptionHandler({MethodArgumentNotValidException.class})
    public ResponseEntity methodArgumentNotValidException(MethodArgumentNotValidException ex){
        return ResponseEntity.badRequest()
                .body(ex.getAllErrors()
                        .stream()
                        .map( e -> e.getDefaultMessage())
                        .collect(Collectors.toList()));
    }

    @ExceptionHandler({StudentNotFoundException.class})
    public ResponseEntity<?> handleUserNotfoundException(StudentNotFoundException ex){
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.NOT_FOUND);
    }
}
