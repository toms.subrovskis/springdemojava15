package com.example.springdemojava15.rest;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Data
@Entity(name = "Students")
public class Student {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="ID")
    private Long id;
    @NotNull
    @Column(name="NAME")
    private String name;
    @Column(name="AGE")
    private int age;

    public Student(){
    }

    public Student(String name, int age){
        this.name=name;
        this.age=age;
    }


}
