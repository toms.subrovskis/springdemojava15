package com.example.springdemojava15.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

//    public void StudentService(StudentRepository studentRepository){
//        this.studentRepository=studentRepository;
//    }

//    Ja izmantojam constructor dependency injection tad šī metode nestrādās + labāk izmantot atsevišķu .sql failu, lai ielādētu datus datubāzē
    @PostConstruct
    public void initDB(){
        studentRepository.save(new Student("Janis", 25));
        studentRepository.save(new Student("Peteris", 74));
        studentRepository.save(new Student("Ieva", 28));
    }

    public List<Student> getAllStudents(){
        return this.studentRepository.findAll();
    }

    public Student getStudent(Long id){
//        Garāks veids kā implementēt getStudent loģiku
//        Optional<Student> optionalStudent = this.studentRepository.findById(id);
//
//        if(optionalStudent.isEmpty()){
//            throw new StudentNotFoundException(id);
//        }
//        Student student = optionalStudent.get();
//
//        return student;

//        Īsāks veids izmantojot lambdas
        return studentRepository.findById(id)
                .orElseThrow( () -> new StudentNotFoundException(id));
    }

    public Student saveStudent(Student student){
        return studentRepository.save(student);
    }

    public Student updateStudent(Long id, Student newStudent){
//        Optional<Student> optionalStudent = this.studentRepository.findById(id);
//        if(optionalStudent.isEmpty()){
//            throw new StudentNotFoundException(id);
//        }
//        Student foundStudent = optionalStudent.get();
//        foundStudent.setName(student.getName());
//        foundStudent.setAge(student.getAge());
//        this.studentRepository.save(foundStudent);

//        Cits pieraksts izmantojot lambda expressions
        return studentRepository.findById(id)
                .map( student -> {
                    student.setName(newStudent.getName());
                    student.setAge(newStudent.getAge());
                    return studentRepository.save(student);
                     })
                .orElseGet( () -> {
                    newStudent.setId(id);
                    return studentRepository.save(newStudent);
                     });
    }

    public void deleteStudent(Long id){
        studentRepository.deleteById(id);
    }
}
