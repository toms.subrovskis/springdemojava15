package com.example.springdemojava15.rest;


import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class TestController {

    private StudentService studentService;

    public TestController(StudentService studentService){
        this.studentService=studentService;
    }

    @GetMapping("/")
    public String sayHello(){
        return "Hello from default path";
    }

    @GetMapping("/{name}")
    public String sayHello(@PathVariable("name") String someName){
        return "Hello "+someName;
    }

    @GetMapping("/students")
    public List<Student> getStudentList(){
        return studentService.getAllStudents();
    }

    @GetMapping("/students/{id}")
    public Student getStudent(@PathVariable("id") Long id){
        return studentService.getStudent(id);
    }

    @PostMapping("/students")
    public Student saveStudent(@Valid @RequestBody Student student){
        return studentService.saveStudent(student);
    }

    @PutMapping("/students/{id}")
    public Student updateStudent(@PathVariable("id") Long id,
                              @RequestBody Student student){
        return studentService.updateStudent(id, student);
    }

    @DeleteMapping("/students/{id}")
    public void deleteStudent(@PathVariable("id") Long id){
        studentService.deleteStudent(id);
    }
}
