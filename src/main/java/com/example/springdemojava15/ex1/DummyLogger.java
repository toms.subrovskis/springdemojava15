package com.example.springdemojava15.ex1;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component("dummyLoggerEx1")
//@Slf4j
public class DummyLogger implements CommandLineRunner {

    private Logger logger = LoggerFactory.getLogger(DummyLogger.class);

    @Override
    public void run(String... args) throws Exception {
        logger.debug("Helo from debug");
        logger.info("Hello from task1");
        logger.warn("Hello from warning");
        logger.error("Hello from error");
    }
}
