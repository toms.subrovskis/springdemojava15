package com.example.springdemojava15.ex5;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * Inject the following scaffold definition of the WelcomeMessageLogger class using the @Value annotation, values, which you will define in the file application.properties using the following keys:
 *
 * pl.sdacademy.welcome.text.value
 * pl.sdacademy.welcome.text.enable
 * The property pl.sdacademy.welcome.text.value defines the text that should be printed at application startup. The default should be none.
 *
 * The property pl.sdacademy.welcome.text.enable defines whether the text defined in pl.sdacademy.welcome.text.value should be displayed on the screen. There is no default value.
 *
 * Inject both properties using the constructor.
 */

@Component
@Slf4j
public class WelcomeMessageLogger implements CommandLineRunner {

    @Value("${pl.sdacademy.welcome.text.value:none}")
    private String text;
    @Value("${pl.sdacademy.welcome.text.enable}")
    private Boolean shouldLog;

    @Override
    public void run(String... args) throws Exception {
        if(shouldLog){
            log.info(text);
        }
    }
}
