package com.example.springdemojava15.ex2;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * Inject the DummyLogger class into:
 *
 * the CommandLineRunnerWithConstructorInjection class using a constructor
 * the CommandLineRunnerWithFieldInjection class directly to the field
 * the CommandLineRunnerWithSetterInjection class using a setter
 */

@Component
@Slf4j
public class DummyLogger {
    public void sayHello(String text) {
        log.info(text);
    }
}

