package com.example.springdemojava15.advancedDependencyInjection;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class SecondManager implements CommandLineRunner {

    private final List<MyService> myServices;

    @Autowired
    public SecondManager(List<MyService> myServices){
        this.myServices=myServices;
    }


    @Override
    public void run(String... args) throws Exception {
        log.info("secondManager starts working");
//        myServices.forEach(MyService::doWork);
        myServices.forEach(service -> service.doWork());
//        for(MyService service : myServices){
//            service.doWork();
//        }
    }
}
