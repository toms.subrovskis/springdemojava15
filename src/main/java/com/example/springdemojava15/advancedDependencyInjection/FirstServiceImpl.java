package com.example.springdemojava15.advancedDependencyInjection;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Qualifier("firstService")
@Slf4j
public class FirstServiceImpl implements MyService{

    @Override
    public void doWork() {
        log.info("First service works");
    }
}
