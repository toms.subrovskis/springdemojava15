package com.example.springdemojava15.advancedDependencyInjection;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Slf4j
public class FirstManager {

    private final MyService myService;

    @Autowired
    public FirstManager(@Qualifier("firstService") MyService myService){
        this.myService=myService;
    }

    @PostConstruct
    public void startWork(){
        log.info("firstManager starts working");
        myService.doWork();
    }
}
