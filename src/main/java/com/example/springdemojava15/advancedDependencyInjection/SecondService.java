package com.example.springdemojava15.advancedDependencyInjection;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@Qualifier("secondService")
public class SecondService implements MyService {

    @Override
    public void doWork() {
        log.info("Second service works");
    }
}
