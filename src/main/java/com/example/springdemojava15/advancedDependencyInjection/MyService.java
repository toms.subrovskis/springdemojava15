package com.example.springdemojava15.advancedDependencyInjection;

public interface MyService {
    void doWork();
}
